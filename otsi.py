#!/usr/bin/python
# -*- coding: utf-8 -*-
#Marko Mandel
#
#Skript loeb esimese parameetrina antud failist sisse URL-id ja nende järel otsis6na. 
#S6na ja URL eraldatakse, seejärel tehakse päring vastava URLi pihta. 
#Vastuse lähtekoodist otsitakse URLi järel olnud s6na. 
#Teise parameetrina antud faili kirjutatakse uuesti URL, 
#otsitav string ja "JAH/EI" vastavalt sellele, kas otsitav string leiti või mitte. 
#
#kui otsis6nu on mitu, siis otsitakse esimest
#NB! Kui URL-i ees ei ole "http" v6i "https", siis lisatakse "http://" automaatselt juurde
import sys
import os
import urllib

#kontrollib kas argumente on 2
if len(sys.argv) != 3:
    print 'Kasutamine: otsi.py <sisend> <v2ljund>'
    sys.exit()

sisendfail = os.path.abspath(sys.argv[1])
v2ljundfail = os.path.abspath(sys.argv[2])

#kontrolli kas sisendfail on olemas
try:
    open(sisendfail)
except IOError:
    print 'Viga: Sisendfaili ei leitud!'
    sys.exit()
else:
    pass

#paneb sisendfaili 2D-massiivi
sisend = open(sisendfail)    
url = []
for line in sisend.readlines():
    line = line.strip()
    url.append(line.split(' '))
sisend.close()

v2ljundfaili = []

for saidinimi in url:
    #kui url ei hakka s6nega 'http', siis pannakse "http://" urli ette, ja proovitakse uuesti
    if saidinimi[0][0:4] != 'http':
        saidinimi[0] = "http://" + saidinimi[0]


    #kontrolli kas veebileht on olemas
    try:
        ajutineobjekt = urllib.urlopen(saidinimi[0])
        saidinimi[1] 
    except NameError:
        print 'Viga: ' + saidinimi[0] + ' veebisaiti pole olemas v6i ei saa sinna yhendust luua!'
        sys.exit()
    except IOError:
        print 'Viga: ' + saidinimi[0] + ' veebisaiti pole olemas v6i ei saa sinna yhendust luua!'
        sys.exit()
    except IndexError:
        print 'Viga: ' + saidinimi[0] + ' veebisaidi taga pole otsis6na!'
        sys.exit()
    else:
        ajutineobjekt.close()

    #salvestab saidi html-koodi 'sait' muutujasse
    yhendus = urllib.urlopen(saidinimi[0])
    sait = yhendus.read() 
    yhendus.close() 
    
    #kirjutab tulemused massiivi
    if saidinimi[1] in sait:
        v2ljundfaili.append(saidinimi[0] + ' ' + saidinimi[1] + ' ' + 'JAH')
    else:
        v2ljundfaili.append(saidinimi[0] + ' ' + saidinimi[1] + ' ' + 'EI')

#kui m6ni kaust failitees puudub, siis need luuakse 
if not os.path.exists(v2ljundfail):
    os.makedirs(os.path.dirname(v2ljundfail))       
        
#loob v2ljundifaili, kui seda veel ei ole. Kirjutab tulemused faili.
file = open(v2ljundfail, "w")
for rida in v2ljundfaili:
    file.write(rida + '\n')
file.close()

print '\nValmis! V2ljund on failis ' + v2ljundfail

